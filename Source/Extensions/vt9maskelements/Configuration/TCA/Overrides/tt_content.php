<?php
$GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['vt9maskelements_vt9card'] = 'tx_vt9maskelements_vt9card';
$GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['vt9maskelements_vt9slider'] = 'tx_vt9maskelements_vt9slider';
$tempColumns = array (
  'tx_vt9maskelements_sliderelement' => 
  array (
    'config' => 
    array (
      'appearance' => 
      array (
        'enabledControls' => 
        array (
          'dragdrop' => '1',
        ),
        'levelLinksPosition' => 'both',
        'newRecordLinkTitle' => 'New Slider Element',
        'useSortable' => '1',
      ),
      'foreign_field' => 'parentid',
      'foreign_sortby' => 'sorting',
      'foreign_table' => 'tx_vt9maskelements_sliderelement',
      'foreign_table_field' => 'parenttable',
      'minitems' => '1',
      'type' => 'inline',
    ),
    'exclude' => '1',
    'label' => 'LLL:EXT:vt9maskelements/Resources/Private/Language/locallang_db.xlf:tt_content.tx_vt9maskelements_sliderelement',
  ),
  'tx_vt9maskelements_vt9cardbuttonlink' => 
  array (
    'config' => 
    array (
      'fieldControl' => 
      array (
        'linkPopup' => 
        array (
          'options' => 
          array (
            'blindLinkOptions' => 'folder',
            'title' => 'Link',
            'windowOpenParameters' => 'height=800,width=700,status=0,menubar=0,scrollbars=1',
          ),
        ),
      ),
      'renderType' => 'inputLink',
      'softref' => 'typolink',
      'type' => 'input',
      'wizards' => 
      array (
        'link' => 
        array (
          'icon' => 'actions-wizard-link',
        ),
      ),
    ),
    'exclude' => '1',
    'label' => 'LLL:EXT:vt9maskelements/Resources/Private/Language/locallang_db.xlf:tt_content.tx_vt9maskelements_vt9cardbuttonlink',
  ),
  'tx_vt9maskelements_vt9cardbuttontext' => 
  array (
    'config' => 
    array (
      'type' => 'input',
    ),
    'exclude' => '1',
    'label' => 'LLL:EXT:vt9maskelements/Resources/Private/Language/locallang_db.xlf:tt_content.tx_vt9maskelements_vt9cardbuttontext',
  ),
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tt_content', $tempColumns);
$GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = [
    'LLL:EXT:vt9maskelements/Resources/Private/Language/locallang_db.xlf:tt_content.CType.div._vt9maskelements_',
    '--div--',
];
$GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = [
    'LLL:EXT:vt9maskelements/Resources/Private/Language/locallang_db.xlf:tt_content.CType.vt9maskelements_vt9card',
    'vt9maskelements_vt9card',
    'tx_vt9maskelements_vt9card',
];
$GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = [
    'LLL:EXT:vt9maskelements/Resources/Private/Language/locallang_db.xlf:tt_content.CType.vt9maskelements_vt9slider',
    'vt9maskelements_vt9slider',
    'tx_vt9maskelements_vt9slider',
];
$tempTypes = array (
  'vt9maskelements_vt9card' => 
  array (
    'columnsOverrides' => 
    array (
      'bodytext' => 
      array (
        'config' => 
        array (
          'richtextConfiguration' => 'default',
          'enableRichtext' => 1,
        ),
      ),
    ),
    'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,header,image,bodytext,tx_vt9maskelements_vt9cardbuttontext,tx_vt9maskelements_vt9cardbuttonlink,--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,--palette--;;language,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,--palette--;;hidden,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,--div--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_category.tabs.category,categories,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,rowDescription,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended,tx_gridelements_container,tx_gridelements_columns, --div--;LLL:EXT:gridelements/Resources/Private/Language/locallang_db.xlf:gridElements',
  ),
  'vt9maskelements_vt9slider' => 
  array (
    'columnsOverrides' => 
    array (
      'bodytext' => 
      array (
        'config' => 
        array (
          'richtextConfiguration' => 'default',
          'enableRichtext' => 1,
        ),
      ),
    ),
    'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,tx_vt9maskelements_sliderelement,--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,--palette--;;language,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,--palette--;;hidden,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,--div--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_category.tabs.category,categories,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,rowDescription,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended,tx_gridelements_container,tx_gridelements_columns, --div--;LLL:EXT:gridelements/Resources/Private/Language/locallang_db.xlf:gridElements',
  ),
);
$GLOBALS['TCA']['tt_content']['types'] += $tempTypes;
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'vt9maskelements',
    'Configuration/TypoScript/',
    'vt9maskelements'
);
