# TYPO3-Projekt aus dem Zoominar "Backend-Benutzer und Benutzergruppen konfigurieren" am 26.01.2021

Sofern Du DDEV benutzt und das Projekt lokal starten willst, klone dir dieses Repository und führe ein

`ddev start`

aus, anschließend ein

`ddev composer install`

Um die Datenbank zu importieren:

`ddev import-db -f database.sql`

Backend starten:

`ddev launch /typo3`

Benutzername: admin
Passwort: password
